import React, { Component } from 'react';
import { Text, View, Image,  StyleSheet } from 'react-native';

import Button from './Button';
import { Actions } from 'react-native-router-flux';


const Menu = () => {
  return (
    <View style={{flex:0.7, flexDirection: 'column'}}>
      <View style={styles.title} >
        <Text style={{fontSize: 30, fontWeight: 'bold'}}>¡Bienvenidos!</Text>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>Final DSM</Text>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Image 
          source={require('../../image/Flickr_logo.png')}
          style= {{width:230, height:70}}/>
      </View>
      <View style={{flex: 1, marginBottom:10}}>
        <Button  onPress={() => Actions.albumList()}>
          Ver álbumes
        </Button>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text style={{fontSize: 20}}>UTN - FRC</Text>
        <Text style={{fontSize: 18}}>Brond, Matías - Silva Federico</Text>
      </View>
    </View>
  );
};

var styles = StyleSheet.create({
  title: {
    flex: 1,
    alignItems: 'center',
    marginRight: 28
  }
});

export default Menu;
