import React from 'react';
import { Text, View, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';
import { Actions } from 'react-native-router-flux';
import { Share } from 'react-native'

const PhotoDetail = ({ title, imageUrl, id }) => {
  const {
    thumbnailStyle,
    headerContentStyle,
    thumbnailContainerStyle,
    headerTextStyle,
    imageStyle
  } = styles;

  return (
    <Card>
      <CardSection>
        <View style={thumbnailContainerStyle}>
          <Image
            style={thumbnailStyle}
            source={{ uri: imageUrl }}
          />
        </View>
        <View style={headerContentStyle}>
          <Text style={headerTextStyle}>{title}</Text>

        </View>
      </CardSection>

      <CardSection>
        <Image
          style={imageStyle}
          source={{ uri: imageUrl }}
        />
      </CardSection>

      <CardSection>
        <Button onPress={() => Actions.photoView({title: title, photoId:id, url: imageUrl}) }>
          Ver comentarios
        </Button>
        <Button onPress={() => Linking.openURL(imageUrl) }>
          Navegador
        </Button>
      </CardSection>
      <CardSection>
        <Button onPress={() => Share.share({
                message: 'Mirá esta imagen de flickr: ' + imageUrl,
                url: 'https://www.flickr.com/',
                title: 'Imagen'
            }, {
                dialogTitle: 'Compartí esta imagen de flickr'
              })
        }>
          Compartir
        </Button>
      </CardSection>
    </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  }
};

export default PhotoDetail;
