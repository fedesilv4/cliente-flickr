// Index.ios.js - place code in here for IOS!!!!

// Import a library to help create a component
import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';
import PhotoList from './src/components/PhotoList';
import PhotoView from './src/components/PhotoView';
import Menu from './src/components/Menu';

import { Router, Scene } from 'react-native-router-flux';


// Create a component
const App = () => (
      <Router>
        <Scene key="root">
          <Scene key="menu" component={Menu} title="Inicio"  initial={true} />
          <Scene key="albumList" component={AlbumList} title="Álbumes"  />
          <Scene key="photoList" component={PhotoList} title="Fotos" />
          <Scene key="photoView" component={PhotoView} title="Ver foto"/>
        </Scene>
      </Router>
);
export default App;
