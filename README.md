# **Cliente de Flickr mobile utilizando React Native**

Aplicación móvil cliente de Flickr implementada con el framework de javascript React Native, presentada como trabajo práctico de Diseño de Software Multipantalla de la Universidad Tecnológica Nacional, Facultad Regional Córdoba.

### Probar la aplicación con expo
1. Clonamos el repositorio
    ```
    $ git clone https://gitlab.com/BeFede/cliente-flickr.git
    ```
2. Nos posicionamos en la carpeta del proyecto
    ```
    $ cd cliente-flickr
    ```
 3. Instalamos las dependencias del proyecto
    ```
    $ npm install
    ```
4. Ejecutamos el proyecto
    ```
    $ npm start
    ```
_Este último paso mostrará en el terminal un código QR para ser tomado desde la aplicación expo y comenzar a utlizar la aplicación_

